package com.example.learn.androidresources;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.learn.R;


public class EnumKeyResources extends Activity {
	Button button1 = null;
	Button button2 = null;
	private static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_enum_key_resources);
		
		//Parse String array
		button1 = (Button) findViewById(R.id.parsestringarray);
		button1.setOnClickListener(parsestringarray);
		
		//2nd Button
		button2 = (Button) findViewById(R.id.get_simple_text);
		button2.setOnClickListener(get_Raw_and_XML);
		
		
	}
	
	 public static Context getContext(){
	        return context;
	    }

	
	
	private OnClickListener parsestringarray = new OnClickListener() {
		public void onClick(View v) {
			//Get access to Resources object from an Activity
			Resources res = context.getResources();
			String strings[] = res.getStringArray(R.array.test_array);
			
			String string = res.getString(R.string.txtSimpleText);
			TextView content1 = (TextView) findViewById(R.id.content1);
			
			//clear content1 field
			content1.setText("");

			
			//append array
			for (String s: strings)
			{
			    Log.d("Parse String Array = ", s);
			    //Toast.makeText(context, s, Toast.LENGTH_LONG).show();
			    content1.append(s+"\n");
			}
			
			
			//append simple text
			content1.append(string+"\n");
			
			//append quoted string
			content1.append(res.getString(R.string.quoted_string) +"\n");
			
			//append double quoted string
			content1.append(res.getString(R.string.double_quoted_string) +"\n");
			
			//append formatted string
			String javaFormatString = res.getString(R.string.java_format_string);			
			String substitutedString = String.format(javaFormatString, "Hello", "Android");
			content1.append(substitutedString);
			
			//Set Spanned HTML Text
			String htmlTaggedString = res.getString(R.string.tagged_string);			
			content1.append(Html.fromHtml(htmlTaggedString+"\n"));
			//content1.setText(Html.fromHtml("<b>Image(s): </b>"));			
			content1.setTextColor(getResources().getColorStateList(R.color.red));
			
			//get the dimension
			float dimen = EnumKeyResources.this.getResources().getDimension(R.dimen.mysize_in_dp);
			content1.setTextSize(1, dimen);
			//append("\nDP="+Float.toString(dimen)+"\n");
			
			//button1.setBackgroundResource(R.drawable.blue_sqr_home_on);
			button1.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.blue_sqr_home_on), null, null, null);

			// Get a drawable
			ColorDrawable blueDrawable = (ColorDrawable)
					EnumKeyResources.this.getResources().getDrawable(R.drawable.blue_rectangle);

			//Set it as a background to a text view
			content1.setBackgroundDrawable(blueDrawable);




			

		}
		
	};	
	
	private OnClickListener get_Raw_and_XML = new OnClickListener() {
		public void onClick(View v) {
			
			//Get access to Resources object from an Activity
			Resources res = context.getResources();
			String string = res.getString(R.string.txtSimpleText);			
			TextView content2 = (TextView) findViewById(R.id.content2);	
			
			
			try {
				String holder = getEventsFromAnXMLFile((Activity) context );
				String holder2 = getStringFromRawFile((Activity) context);
				String holder3 = getStringFromAssetFile((Activity) context);
				//Toast.makeText(context, holder, Toast.LENGTH_LONG).show();
				content2.setText(holder);
				content2.append("\n\n"+holder2);
				content2.append("\n\n"+holder3);
				
				
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			

		}
		
	};	
	
	
	
	
	private String getEventsFromAnXMLFile(Activity activity)
			throws XmlPullParserException, IOException
			{
			   StringBuffer sb = new StringBuffer();
			   Resources res = activity.getResources();
			   XmlResourceParser xpp = res.getXml(R.xml.test);

			   xpp.next();
			   int eventType = xpp.getEventType();
			     while (eventType != XmlPullParser.END_DOCUMENT)
			    {
			         if(eventType == XmlPullParser.START_DOCUMENT)
			         {
			           sb.append("******Start document");
			         }
			         else if(eventType == XmlPullParser.START_TAG)
			         {
			           sb.append("\nStart tag "+xpp.getName());
			         }
			         else if(eventType == XmlPullParser.END_TAG)
			         {
			            sb.append("\nEnd tag "+xpp.getName());
			         }
			         else if(eventType == XmlPullParser.TEXT)
			         {
			           sb.append("\nText "+xpp.getText());
			         }
			         eventType = xpp.next();
			    }//eof-while
			    sb.append("\n******End document");
			    return sb.toString();
			}//eof-function
	
	
	

//read text from raw
	String getStringFromRawFile(Activity activity)
			   throws IOException
			   {
			      Resources r = activity.getResources();
			      InputStream is = r.openRawResource(R.raw.raw);
			      String myText = convertStreamToString(is);
			      is.close();
			      return myText;
			   }

			   String convertStreamToString(InputStream is)
			   throws IOException
			   {
			      ByteArrayOutputStream baos = new ByteArrayOutputStream();
			      int i = is.read();
			      while (i != -1)
			      {
			         baos.write(i);
			         i = is.read();
			      }
			      baos.flush();
			      return baos.toString();
			    }
	


//get assets
			   String getStringFromAssetFile(Activity activity) throws IOException
			   {
			       AssetManager am = activity.getAssets();
			       InputStream is = am.open("test.txt");
			       String s = convertStreamToString(is);
			       is.close();
			       return s;
			   }

}
