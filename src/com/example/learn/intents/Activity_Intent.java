package com.example.learn.intents;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.preference.PreferenceManager.OnActivityResultListener;
import android.provider.ContactsContract;
import android.provider.Contacts.Settings;
import android.provider.ContactsContract.PhoneLookup;
import android.text.Editable;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.learn.uicontrols.VideoViewer;
import com.example.learn.utils.IntentsUtils;
import com.example.learn.utils.Utility;

import com.example.learn.R;

public class Activity_Intent extends Activity implements OnClickListener{
	Button button1 = null;
	Button button2 = null;
	Button button3 = null;
	Button button3a = null;
	Button button3b = null;
	Button button3c = null;
	Button button3d = null;
	Button button3e = null;
	Button button3f = null;
	Button button3g = null;
	Button button3h = null;
	Button button3i = null;
	Button button3j = null;
	Button button3k = null;
	Button button3l = null;
	Button button3m = null;
	Button button3n = null;
	Button button3o = null;
	Button button3p = null;
	Button button3s = null;
		
	TextView mResults = null;
	TextView text2 = null;
	ImageView iv = null;
	private static Context context;
	private OnSharedPreferenceChangeListener listener;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity__intent);
		
		button1 = (Button) findViewById(R.id.button1);
		//button1.setOnClickListener(Button1_Listener);
		button1.setOnClickListener(this);
		registerForContextMenu(button1);
		
		button2 = (Button) findViewById(R.id.button2);
		button2.setOnClickListener(Button2_Listener);
		
		button3 = (Button) findViewById(R.id.button3);
		button3.setOnClickListener(Button3_Listener);
		
		button3a = (Button) findViewById(R.id.button3a);
		button3a.setOnClickListener(Button3a_Listener);
		
		button3b = (Button) findViewById(R.id.button3b);
		button3b.setOnClickListener(Button3b_Listener);
		
		button3c = (Button) findViewById(R.id.button3c);
		button3c.setOnClickListener(Button3c_Listener);
		
		button3d = (Button) findViewById(R.id.button3d);
		button3d.setOnClickListener(Button3d_Listener);
		
		button3e = (Button) findViewById(R.id.button3e);
		button3e.setOnClickListener(Button3e_Listener);		
		
		button3f = (Button) findViewById(R.id.button3f);
		button3f.setOnClickListener(this);
		
		button3g = (Button) findViewById(R.id.button3g);
		button3g.setOnClickListener(this);
		
		button3h = (Button) findViewById(R.id.button3h);
		button3h.setOnClickListener(this);
		
		button3i = (Button) findViewById(R.id.button3i);
		button3i.setOnClickListener(this);
		
		button3j = (Button) findViewById(R.id.button3j);
		button3j.setOnClickListener(this);
		
		button3k = (Button) findViewById(R.id.button3k);
		button3k.setOnClickListener(this);
		
		button3l = (Button) findViewById(R.id.button3l);
		button3l.setOnClickListener(this);
		
		button3m = (Button) findViewById(R.id.button3m);
		button3m.setOnClickListener(this);
		
		button3n = (Button) findViewById(R.id.button3n);
		button3n.setOnClickListener(this);
		
		button3o = (Button) findViewById(R.id.button3o);
		button3o.setOnClickListener(this);
		
		button3p = (Button) findViewById(R.id.button3p);
		button3p.setOnClickListener(this);
		
		button3s = (Button) findViewById(R.id.button3s);
		button3s.setOnClickListener(this);
		
		// Retrieve the TextView widget that will display results.
        mResults = (TextView) findViewById(R.id.editText1);

        // This allows us to later extend the text buffer.
        mResults.setText(mResults.getText(), TextView.BufferType.EDITABLE);
        
        text2 = (TextView) findViewById(R.id.editText2);        
        text2.append("\nhttp://www.google.com\n");
        text2.append("anthony@gmail.com\n");
        text2.append("555-555-5555\n");
        Linkify.addLinks(text2, Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES);
        
        
        //Preferences stuff
        SharedPreferences settings = getSharedPreferences("MYPREFS", 0);
        mResults.setText(settings.getString("tvalue", ""));
        Log.d("Meikle-Prefs-onCreate", settings.getString("tvalue", ""));
        
        
        
        /**
        //Preference Listener
        SharedPreferences settings_xx = PreferenceManager.getDefaultSharedPreferences(this);
        listener = new OnSharedPreferenceChangeListener() {
			
			@Override
			public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
					String key) {
				Activity_Intent.this.displayPrefsViaMethod(null);				
			}
		};
		settings_xx.registerOnSharedPreferenceChangeListener(listener);
        **/
        
        
        
	}
	
	public static Context getContext(){
        return context;
    }
	
	//For when you hit the menu option
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_activity__intent_2, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		if(item.getItemId() == R.id.menu_settings){
			Log.d("Meikle", "Menu Settings clicked");
		} else if(item.getItemId() == R.id.menu_Prefs){
			Log.d("Meikle", "Preferences clicked");
		}		
		return false;		
	};
	
	
	//For Long clicks
	@Override
	public void onCreateContextMenu(android.view.ContextMenu menu, View v, android.view.ContextMenu.ContextMenuInfo menuInfo) {
		getMenuInflater().inflate(R.menu.activity_activity__intent_2_radio, menu);
		
	};	
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.menu_settings){
			Log.d("Meikle", "Context Menu Settings clicked");
		} else if(item.getItemId() == R.id.menu_Prefs){
			Log.d("Meikle", "Context Preferences clicked");
		}
		return false;
	
	}
	
	
	@Override
	public void onClick(View v) {
		Log.d("Meikle", "Here....");
		if (v.getId() == R.id.button1) {
			//Log.d("Meikle", "Button 1 clicked");
		}
		
		switch(v.getId()){
		case R.id.button1: 
			//Dials a number
			Log.d("Meikle", "Button 1 clicked");
			IntentsUtils.dial((Activity) context);	
			break;
		case R.id.button3f:
			//Alert Dialog with YES/NO prompt
			Log.d("Meikle", "Button Alert Dialog clicked");
			AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Intent.this);
			builder.setMessage("Are you sure you want to exit");
			builder.setCancelable(false);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Activity_Intent.this.finish();					
				}
				
			});
			
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();			
				}
				
			});
			
			AlertDialog alert = builder.create();
			alert.show();
			break;
			
		case R.id.button3g:
			//Starts a progress dialog 
			Log.d("Meikle", "Button Start Progress clicked");
			ProgressDialog pd = new ProgressDialog(this);
			pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pd.setMessage("Just Wait....");
			pd.setIndeterminate(true);
			pd.setCancelable(true);
			pd.show();
			break;
			
		case R.id.button3h:
			//Starts a new activity that is based on the theme dialog
			Dialog d = new Dialog(Activity_Intent.this);
			d.setContentView(R.layout.dialog);
			d.setTitle("Yeah baby!");
			d.show();
			break;
			
		case R.id.button3i:
			//Displays a notification coupled with a sound;
			NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notify = new Notification(android.R.drawable.stat_notify_more, "sigh!!", System.currentTimeMillis());
			CharSequence title = "You have been notified";
			CharSequence details = "More details will be added here";
			Intent intent = new Intent(context, Activity_Intent.class );
			PendingIntent pend = PendingIntent.getActivity(context, 0, intent, 0);
			notify.setLatestEventInfo(context, title, details, pend);
			notify.sound = Uri.parse("android.resource://com.example.learn/" + R.raw.lowbattery);
			nm.notify(0, notify);
			break;
			
		case R.id.button3j:
			//Starts the VideoView activity which plays a video in the raw folder
			Intent intent_vid = new Intent(Activity_Intent.this, VideoViewer.class);
			startActivity(intent_vid);
    		break;
			
		case R.id.button3k:
			//Starts the camera media store and returns the photo taken
			iv = (ImageView) findViewById(R.id.imageView1);
			Intent intent_photo = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent_photo, 1);			
    		break;
    		
		case R.id.button3l:
			//Starts the preferences screen
			Intent intent_pref = new Intent(Activity_Intent.this, Preferences.class);
			startActivity(intent_pref);
			break;
			
		case R.id.button3m:
			//Displays the value from the SharedPreferences "first"
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
			boolean first = settings.getBoolean("first", false);
			Toast.makeText(Activity_Intent.this, new Boolean(first).toString(), Toast.LENGTH_SHORT).show();
			break;
			
		case R.id.button3n:
			//Creates and populates a simple SQLLIte DB
			Log.d("Meikle", "Creating Table and stuff");
			SQLiteDatabase db = openOrCreateDatabase("MyDB", MODE_PRIVATE, null);
			db.execSQL("CREATE TABLE IF NOT EXISTS MyTable (LastName VARCHAR, FirstName VARCHAR, Age INT(3));");
			db.execSQL("INSERT INTO MyTable VALUES ('Meikle', 'Anthony', 36);");
			db.close();
			Log.d("Meikle", "Finished creating Table and stuff");
			break;
			
		case R.id.button3o:
			//Reads data from the SQLLite DB
			SQLiteDatabase db_o = openOrCreateDatabase("MyDB", MODE_PRIVATE, null);
			Cursor c = db_o.rawQuery("SELECT * FROM MyTable", null);
			c.moveToFirst();
			String fname = c.getString(c.getColumnIndex("FirstName"));
			String lname = c.getString(c.getColumnIndex("LastName"));
			int age = c.getInt(c.getColumnIndex("Age"));			
			Toast.makeText(Activity_Intent.this, fname +" " + lname+ " "+ new Integer(age).toString(), Toast.LENGTH_SHORT).show();
			db_o.close();
			break;
			
		case R.id.button3p:
			//Send to logcat all the names within the contacts db
			Cursor people = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
			while(people.moveToNext()){
				int nameIndex = people.getColumnIndex(PhoneLookup.DISPLAY_NAME);
				String name = people.getString(nameIndex);
				Log.d("Meikle", name);
			}			
			break;
			
		case R.id.button3s:
			//Toast the apps internal absolute path
			File f = getFilesDir();
			String path = f.getAbsolutePath();
			Toast.makeText(Activity_Intent.this, "Internal\n" +path, Toast.LENGTH_SHORT).show();
			Log.d("Meikle", "Internal\n" +path);
			
			//Toast the apps external absolute path
			File f_ext = getExternalFilesDir(null);
			String path_ext = f_ext.getAbsolutePath();
			Toast.makeText(Activity_Intent.this, "External\n" +path_ext, Toast.LENGTH_SHORT).show();
			Log.d("Meikle", "External\n" +path_ext);
			
			//now create an external folder
			File ext_Dir = new File(path_ext, "new_folder");
			ext_Dir.mkdir();
			
			//Toast the systems external storage state
			if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
				Toast.makeText(Activity_Intent.this, "External storage is mounted and available " , Toast.LENGTH_SHORT).show();
			}
			
			
			
		default:
			Log.d("Meikle", "default...");
			break;
		}

		
	}
	

	
	private OnClickListener Button1_Listener = new OnClickListener() {
		public void onClick(View v) {
			
			try {				
				IntentsUtils.dial((Activity) context);		
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	};	
	
	private OnClickListener Button2_Listener = new OnClickListener() {
		public void onClick(View v) {
			
			try {				
				IntentsUtils.call((Activity) context);		
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	};	
	
	private OnClickListener Button3_Listener = new OnClickListener() {
		public void onClick(View v) {
			
			try {	
				ArrayList<String> toppings = new ArrayList<String>();
				toppings.add("Cheese");
				toppings.add("Peperoni");
				toppings.add("Black Olives");				  
				
				Intent intent = new Intent();
				Bundle b = new Bundle();    		    
	    		b.putString("txtContent", "Hello Anthony, this is a simple string");  
	    		b.putStringArrayList("urunler", toppings);
	    		intent.putExtras(b);    		
	    		intent.setClass(Activity_Intent.this, Activity_Intent_2.class);
	    		startActivity(intent);
	    		
			} catch (Exception e) {
				e.printStackTrace();
			}
		}		
	};	


	private OnClickListener Button3a_Listener = new OnClickListener() {
		public void onClick(View v) {
			final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
			 
			emailIntent .setType("plain/text");
			 
			emailIntent .putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"ameikle@gmail.com"});
			 
			emailIntent .putExtra(android.content.Intent.EXTRA_SUBJECT, "Re: "+Activity_Intent.this.getString(R.string.app_name) );
			 
			emailIntent .putExtra(android.content.Intent.EXTRA_TEXT, "");
			 
			startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			

		}
	};
	
	private OnClickListener Button3b_Listener = new OnClickListener() {
		public void onClick(View v) {			
			
			Intent i = new Intent();
			i.setAction(Intent.ACTION_VIEW);
			i.setData(Uri.parse("content://contacts/people/"));
			startActivity(i);

		}
	};
	
	private OnClickListener Button3c_Listener = new OnClickListener() {
		public void onClick(View v) {
			//Tests if App is installed
			try {
				ApplicationInfo info = getPackageManager().getApplicationInfo("com.facebook.katana",0);
				Utility.makeToast((Activity) context, "App_Installed: "+info.dataDir);
			} catch (PackageManager.NameNotFoundException e) {
				Utility.makeToast((Activity) context, "Not Installed");
			}
			
			
			
		}
	};
	
	private OnClickListener Button3d_Listener = new OnClickListener() {
		public void onClick(View v) {
			//
			try {
				//----
				Intent intent = new Intent(Activity_Intent.this, SendResult.class);
                startActivityForResult(intent, 0);
			} catch (Exception e) {
				Utility.makeToast((Activity) context, "Error Button3d_Listener");
			}			
		}
	};
	
	private OnClickListener Button3e_Listener = new OnClickListener() {
		public void onClick(View v) {
			//
			try {
				//----
				 //Send Broadcast
				System.out.println("SendBroacast Attached");
		        Intent intent = new Intent("com.meikle.MyBroadcastReceiver");
		        intent.putExtra(MyBroadcastReceiver.EXTRA_LIFEFORM_NAME,
		                        "facehugger");
		        intent.putExtra(MyBroadcastReceiver.EXTRA_LONGITUDE,
		        		-84.0101);
		        intent.putExtra(MyBroadcastReceiver.EXTRA_LATITUDE,
		        		33.9543);
		        intent.putExtra("message", "Hello world");

		        sendBroadcast(intent);
				
				
			} catch (Exception e) {
				Utility.makeToast((Activity) context, "Error Button3e_Listener");
			}			
		}
	};
	
	private OnClickListener Button3f_Listener = new OnClickListener() {
		public void onClick(View v) {
			//
			try {
				//----
			} catch (Exception e) {
				Utility.makeToast((Activity) context, "Error Button3f_Listener");
			}			
		}
	};
	
	
	
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	 /**
         * This method is called when the sending activity has finished, with the
         * result it supplied.
         */
        if (requestCode == 0) {

            // We will be adding to our text.
            Editable text = (Editable)mResults.getText();

            // This is a standard resultCode that is sent back if the
            // activity doesn't supply an explicit result.  It will also
            // be returned if the activity failed to launch.
            if (resultCode == RESULT_CANCELED) {
                text.append("(cancelled)");

            // Our protocol with the sending activity is that it will send
            // text in 'data' as its result.
            } else {
                text.append("(okay ");
                text.append(Integer.toString(resultCode));
                text.append(") ");
                if (data != null) {
                    text.append(data.getAction());
                }
            }

            text.append("\n\n");
        }
        
        
        if (requestCode == 1) {
        	Bitmap bm = (Bitmap) data.getExtras().get("data");
        	iv.setImageBitmap(bm);
        }
    }
    
    //Called from the onclick xml call
    public void setPrefsviaMethod(View v) {
    	Log.d("Meikle", "Entering setPrefsviaMethod");
    	SharedPreferences settings2 = getPreferences(MODE_PRIVATE);
    	SharedPreferences.Editor editor = settings2.edit();    	
        EditText text = (EditText) findViewById(R.id.editText3);
    	String prefValue = text.getText().toString();
    	editor.putString("username", prefValue);
    	editor.commit();
    	Toast.makeText(Activity_Intent.this, "Preference Saved", Toast.LENGTH_SHORT).show();
    	
    }
    
    //Called from the onclick xml call
    public void displayPrefsViaMethod(View v){
    	Log.d("Meikle", "Entering displayPrefsViaMethod");
    	SharedPreferences settings2 = getPreferences(MODE_PRIVATE);
    	String prefValue = settings2.getString("username", "Not Found");    	
    	TextView tv = (TextView) findViewById(R.id.text_pref_display);
    	tv.setText(prefValue);
    	
    	
    }
    
    //Called from onclick xml.  Creates a file on disk 
    public void createFile(View v) throws IOException, JSONException{
    	JSONArray data = new JSONArray();
    	JSONObject tour;
    	
    	tour = new JSONObject();
    	tour.put("tour", "Tour Jamaica");
    	tour.put("price", 500);
    	data.put(tour);
    	
    	tour = new JSONObject();
    	tour.put("tour", "Tour Kingston");
    	tour.put("price", 600);
    	data.put(tour);
    	
    	tour = new JSONObject();
    	tour.put("tour", "Tour Port Land");
    	tour.put("price", 1700);
    	data.put(tour);
    	
    	String text = data.toString();
    	
    	
    	//EditText text = (EditText) findViewById(R.id.editText3);
    	FileOutputStream fos = openFileOutput("MyFile.txt", MODE_PRIVATE);
    	//fos.write(text.getText().toString().getBytes());
    	fos.write(text.getBytes());
    	fos.close();
    	Toast.makeText(Activity_Intent.this, "File saved to disk\n"+data.toString(), Toast.LENGTH_SHORT).show();    	
    }
    
    //Called from onclick xml.  Reads file on disk 
    public void readFile(View v) throws IOException, JSONException{    	
    	FileInputStream fis = openFileInput("MyFile.txt");
    	BufferedInputStream bis = new BufferedInputStream(fis);
    	StringBuffer b = new StringBuffer();
    	while (bis.available() !=0) {
    		char c = (char) bis.read();
    		b.append(c);			
		}
    	
    	bis.close();
    	fis.close();
    	Log.d("Meikle", b.toString());
    	
    	//-------------------------------------
    	StringBuffer tourbuffer = new StringBuffer();
    	
    	JSONArray data = new JSONArray(b.toString());
    	
    	for (int i = 0; i < data.length(); i++) {
    		String tour = data.getJSONObject(i).getString("tour");
    		int price  = data.getJSONObject(i).getInt("price");
    		tourbuffer.append("\n"+tour + " - "+ price);
		}
    	
    	Toast.makeText(Activity_Intent.this, tourbuffer.toString(), Toast.LENGTH_LONG).show();
    	    	
    }


	@Override
	protected void onStop() {
		super.onStop();
		Log.d("Meikle-onStop", mResults.getText().toString());
		SharedPreferences settings = getSharedPreferences("MYPREFS", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("tvalue", mResults.getText().toString());
        editor.commit(); 
	}

}
