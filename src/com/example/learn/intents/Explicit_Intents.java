package com.example.learn.intents;

import com.example.learn.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

public class Explicit_Intents extends Activity {
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.explixit_intent);
		
		ImageView iv = (ImageView) findViewById(R.id.imageView1);
		iv.setImageURI((Uri) getIntent().getExtras().get(Intent.EXTRA_STREAM));
		 
	}

}
