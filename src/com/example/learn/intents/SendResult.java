package com.example.learn.intents;

import com.example.learn.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SendResult extends Activity{
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_result);
		
		Button bkiana = (Button)findViewById(R.id.kiana);
        bkiana.setOnClickListener(mKianaListener);
        Button baj = (Button)findViewById(R.id.aj);
        baj.setOnClickListener(mAJListener);
        Button bkara = (Button)findViewById(R.id.kara);
        bkara.setOnClickListener(mKaraListener);
        
	}
	
	  private OnClickListener mKianaListener = new OnClickListener()
	    {
	        public void onClick(View v)
	        {
	            // To send a result, simply call setResult() before your
	            // activity is finished.
	            setResult(RESULT_OK, (new Intent()).setAction("I'm Kiana Meikle"));
	            finish();
	        }
	    };

	    private OnClickListener mAJListener = new OnClickListener()
	    {
	        public void onClick(View v)
	        {
	            // To send a result, simply call setResult() before your
	            // activity is finished.
	            setResult(RESULT_OK, (new Intent()).setAction("Boy does AJ love to say Daddy"));
	            finish();
	        }
	    };
	    
	    private OnClickListener mKaraListener = new OnClickListener()
	    {
	        public void onClick(View v)
	        {
	            // To send a result, simply call setResult() before your
	            // activity is finished.
	            setResult(RESULT_OK, (new Intent()).setAction("Give me Milk!!"));
	            finish();
	        }
	    };
	
	

}
