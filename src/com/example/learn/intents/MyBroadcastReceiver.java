package com.example.learn.intents;

import java.util.ArrayList;

import com.example.learn.utils.Utility;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver{
	public final static String EXTRA_LIFEFORM_NAME
    = "EXTRA_LIFEFORM_NAME";
  public final static String EXTRA_LATITUDE = "EXTRA_LATITUDE";
  public final static String EXTRA_LONGITUDE = "EXTRA_LONGITUDE";

  public static final String ACTION_BURN = "com.example.learn.intents.Activity_Intent_2";
  //public static final String NEW_LIFEFORM = "com.paad.alien.action.NEW_LIFEFORM";
  
  
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		// Get the lifeform details from the intent.
        Uri data = intent.getData();
        String type = intent.getStringExtra(EXTRA_LIFEFORM_NAME);
        double lat = intent.getDoubleExtra(EXTRA_LATITUDE, 0);
        double lng = intent.getDoubleExtra(EXTRA_LONGITUDE, 0);
        Location loc = new Location("gps");
        loc.setLatitude(lat);
        loc.setLongitude(lng);        
        Toast.makeText(context, "before entering if statement", Toast.LENGTH_LONG).show();
        System.out.println("before entering if statement");
        if (type.equals("facehugger")) {
        	Toast.makeText(context, "after entering if statement", Toast.LENGTH_LONG).show();
            System.out.println("after entering if statement");
          Intent startIntent = new Intent(context, Activity_Intent_2.class);
          startIntent.putExtra(EXTRA_LATITUDE, lat);
          startIntent.putExtra(EXTRA_LONGITUDE, lng);          
          
          ArrayList<String> toppings = new ArrayList<String>();
		  toppings.add("Cheese");
		  toppings.add("Peperoni");
		  toppings.add("Black Olives");				  
		  startIntent.putExtra("urunler", toppings);
				   
	    		
	    		

          context.startService(startIntent);
		
	}

}
	
}
