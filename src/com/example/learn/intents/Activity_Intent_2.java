package com.example.learn.intents;

import java.util.ArrayList;

import com.example.learn.R;
import com.example.learn.R.layout;
import com.example.learn.R.menu;
import com.example.learn.utils.Utility;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class Activity_Intent_2 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity__intent_2);
		
		Bundle bundle = getIntent().getExtras();        
	    String BundleValue = bundle.getString("txtContent");
	    ArrayList<String> BundleValue2 = bundle.getStringArrayList("urunler");
	    TextView textview = (TextView) findViewById(R.id.text01);
	    
	    
	    textview.setText(BundleValue);
	    System.out.println("Activity_Intent_2 Start");
	    for(String s : BundleValue2){
	    	textview.append("\n"+s);
	    	Utility.makeToast(Activity_Intent_2.this, s);
	    }
	     
	     
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_activity__intent_2, menu);
		return true;
	}

}
