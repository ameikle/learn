package com.example.learn.ui;

import com.example.learn.R;
import com.example.learn.R.layout;
import com.example.learn.R.menu;
import com.example.learn.androidresources.EnumKeyResources;
import com.example.learn.intents.Activity_Intent;
import com.example.learn.uicontrols.ListViewCustom;
import com.example.learn.uicontrols.ListViewStandard;
import com.example.learn.xml.Main;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class UI1 extends Activity {
	TextView textView1, textView3, textView4;
	private static Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ui1);
		
		textView1 = (TextView) findViewById(R.id.text01);
		textView1.setOnClickListener(parsestringarray);
		
		textView3 = (TextView) findViewById(R.id.text03);
		textView3.setOnClickListener(StartActivity_Activity);
		
		textView4 = (TextView) findViewById(R.id.text04);
		textView4.setOnClickListener(StartActivity_4);
		
	}
	
	 public static Context getContext(){
	        return context;
	    }
	 
	
	private OnClickListener parsestringarray = new OnClickListener() {
		public void onClick(View v) {
			
			//Launch enum key resources activity
			Intent intent = new Intent();
			intent.setClass(UI1.this, EnumKeyResources.class);		
			startActivity(intent);
			
			

		}
		
	};	
	
	private OnClickListener StartActivity_Activity = new OnClickListener() {
		public void onClick(View v) {
			
			//Launch enum key resources activity
			Intent intent = new Intent();
			intent.setClass(UI1.this, Activity_Intent.class);		
			startActivity(intent);			
		}		
	};	
	

	private OnClickListener StartActivity_4 = new OnClickListener() {
		public void onClick(View v) {
			
			Intent intent = new Intent();
			intent.setClass(UI1.this, ListViewCustom.class);		
			startActivity(intent);			
		}		
	};	
	
	public void startParsingXML(View v){
		Intent intent = new Intent();
		intent.setClass(UI1.this, Main.class);		
		startActivity(intent);
		
	}
	

}
