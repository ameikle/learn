package com.example.learn.uicontrols;

import com.example.learn.R;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoViewer extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.videoviewer);
		VideoView v = (VideoView) findViewById(R.id.videoView1);
		v.setVideoURI(Uri.parse("android.resource://com.example.learn/" + R.raw.video));
		
		v.setMediaController(new MediaController(this));
		v.start();
		v.requestFocus();
		
	}

}
