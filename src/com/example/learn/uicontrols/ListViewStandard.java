package com.example.learn.uicontrols;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import com.example.learn.R;


public class ListViewStandard extends ListActivity {
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listviewstandard);
		
		setListAdapter(new ArrayAdapter<String>(this, 
				android.R.layout.simple_list_item_1, 
				getResources().getStringArray(R.array.test_array)));
	}
	

}

