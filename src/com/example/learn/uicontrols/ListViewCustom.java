package com.example.learn.uicontrols;

import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.learn.R;

public class ListViewCustom extends ListActivity {

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listviewstandard);
		
		setListAdapter(new MyAdapter(this, 
				android.R.layout.simple_list_item_1,R.id.textView1, 
				getResources().getStringArray(R.array.test_array)));
	}
	
	
	private class MyAdapter extends ArrayAdapter<String>{

		public MyAdapter(Context context, int resource, int textViewResourceId,
				String[] strings) {
			super(context, resource, textViewResourceId, strings);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflator.inflate(R.layout.listviewcustom, parent, false);
			String[] items = getResources().getStringArray(R.array.test_array);
			ImageView iv = (ImageView) row.findViewById(R.id.imageView1);
			TextView tv = (TextView) row.findViewById(R.id.textView1);
			tv.setText(items[position]);
			
			if(items[position].equals("USA - Array 4th Value")){
				iv.setImageResource(R.drawable.usa);
			}else if(items[position].equals("Russia - Array 3rd Value")){
				iv.setImageResource(R.drawable.russia);
			}else if(items[position].equals("Japan - Array 2nd Value")){
				iv.setImageResource(R.drawable.japan);
			}else if(items[position].equals("Brasil - Array 1st Value")){
				iv.setImageResource(R.drawable.brasil);
			}
			return row;
		}
		
	}
	
}
